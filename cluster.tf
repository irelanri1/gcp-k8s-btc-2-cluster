

# GKE cluster
resource "google_container_cluster" "primary" {
  name     = "${var.project_id}-gke"
  
  location   = var.region 

  authenticator_groups_config {
    security_group = "gke-security-groups@reefsolutions.co.uk"
  }
  
  initial_node_count       = 1
  
  #Introduce dedicated, non-default VPC in the future
  #network    = google_compute_network.vpc.name
  #subnetwork = google_compute_subnetwork.subnet.name
}

