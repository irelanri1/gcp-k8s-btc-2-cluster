# gcp-k8s-btc-2-cluster

Creates an `AutoPilot` type Kubernetes cluster in GKE. This gives you a *free* tier cluster with 1 node in each Availability Zone.

( Based upon [this sample project](https://learn.hashicorp.com/tutorials/terraform/gke?in=terraform/kubernetes), and simplified! The Kubernetes dashboard is discouraged by GKE due to security concerns (presumably), so this is omitted - and the GKE dashboard will have to suffice for our purposes.)

Used [this article](https://cloud.google.com/kubernetes-engine/docs/how-to/role-based-access-control#groups-setup-gsuite) to prepare the cluster for integration with Google Groups so that these groups can be used to select which user accounts have what permissions on the cluster.

Used the Google Admin console to create the required parent group against a domain I own, and then added a group to it representing deployment purposes.

Since it is not recommended to mix Terraform providers (Google and Kubernetes in this case), I have had to leave all basic security resources such as Namespaces and Roles, to subsequent projects.


